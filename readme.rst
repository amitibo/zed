ZED cython wrapper
==================

Introduction
------------

ZED is Stereolab_ stereo camera. 
This repository has cython bindings for the ZED sdk. It is
based on the cython-cmake-example_ code.

The project is tested on the *Jetson-TK1* development board. The bindings are
still in development and expose only part of the ZED SDK.

Dependencies
------------

Build Dependencies
^^^^^^^^^^^^^^^^^^

- Python_
- Cython_
- CMake_
- C++ compiler (g++ for instance)
- `ZED SDK`_
- `CUDA SDK`_
- `OpenCV`_ and its python bindings

Build Instructions
------------------

::

  mkdir build
  cd build
  cmake ..
  make
  python setup.py install
  
.. warning::

  In your CMake configuration, make sure that PYTHON_LIBRARY,
  PYTHON_INCLUDE_DIR, and CYTHON_EXECUTABLE are all using the same CPython
  version.

Basic Usage
-----------
See the examples in the folder 'examples'.

License
-------
See included license file.

.. _Stereolab: https://www.stereolabs.com/
.. _cython-cmake-example: https://github.com/thewtex/cython-cmake-example
.. _Cython: http://cython.org/
.. _CMake:  http://cmake.org/
.. _Python: http://python.org/
.. _ZED SDK: https://www.stereolabs.com/developers/
.. _CUDA SDK: https://developer.nvidia.com/cuda-downloads
.. _OpenCV: http://opencv.org/
