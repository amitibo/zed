from libcpp.string cimport string
from libcpp cimport bool

cdef extern from "zed/Camera.hpp" namespace "sl::zed":

    ctypedef unsigned char uchar

    cpdef enum ERRCODE:
        SUCCESS
        NO_GPU_COMPATIBLE
        NOT_ENOUGH_GPUMEM
        ZED_NOT_AVAILABLE
        AUTO_CALIBRATION_FAILED
        INVALID_SVO_FILE
        RECORDER_ERROR

    ctypedef enum MODE:
        NONE
        PERFORMANCE
        QUALITY

    cpdef enum VIEW_MODE:
        STEREO_LEFT
        STEREO_RIGHT
        STEREO_ANAGLYPH
        STEREO_DIFF
        STEREO_SBS
        STEREO_OVERLAY

    cpdef enum SIDE:
        LEFT
        RIGHT

    cpdef enum ZEDResolution_mode:
        HD2K
        HD1080
        HD720
        VGA

    cpdef enum MEASURE:
        DISPARITY
        DEPTH
        CONFIDENCE
        XYZ
        XYZRGBA

    cpdef enum SENSING_MODE:
        FULL
        RAW

    cpdef enum DATA_TYPE:
        FLOAT
        UCHAR

    cpdef enum MAT_TYPE:
        CPU
        GPU

    cdef cppclass Mat:
        Mat()
        Mat(int, int, int, DATA_TYPE, uchar*, MAT_TYPE)
        int width
        int height
        int step
        int channels
        uchar* data
        DATA_TYPE data_type

    cdef cppclass resolution:
        resolution(int, int)
        int width, height

    cdef cppclass Camera:
        Camera(string) except +
        Camera(ZEDResolution_mode, float, int) except +
        resolution getImageSize()
        ERRCODE init(MODE, int, bool, bool)
        bool grab(SENSING_MODE, bool, bool)
        Mat retrieveImage(SIDE)
        Mat retrieveMeasure(MEASURE)
        Mat normalizeMeasure(MEASURE, float, float)
        int getDepthClampValue()
        void setDepthClampValue(int)
        Mat getView(VIEW_MODE)
