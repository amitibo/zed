#!/usr/bin/env python
from __future__ import division
import numpy as np
import time
import cv2
import zed
import os


def main():

    cam = zed.ZEDCamera(resolution_mode=zed.VGA)

    cv2.namedWindow('Left')
    while cv2.waitKey(1) < 0:
        t0 = time.time()

        while cam.grab(zed.FULL, compute_measure=False, compute_disparity=False):
            continue

        img = cam.retrieveImage()
        cv2.imshow('Left', img)

        print 1/(time.time()-t0)

    cv2.destroyAllWindows()


if __name__ == '__main__':
    main()
