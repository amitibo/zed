#!/usr/bin/env python
from __future__ import division
import numpy as np
import cv2
import zed
import sys


def main(img_path):

    cam = zed.ZEDCamera(resolution_mode=zed.VGA)

    cv2.namedWindow('Depth')
    while cv2.waitKey(1) < 0:
        while cam.grab(zed.FULL):
            pass

        normalized = cam.normalizeDepth()
        cv2.imshow('Depth', normalized)

    cv2.imwrite(img_path, normalized)
    cv2.destroyAllWindows()

if __name__ == '__main__':
    main(sys.argv[1])
