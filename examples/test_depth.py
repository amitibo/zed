#!/usr/bin/env python
from __future__ import division
import numpy as np
import time
import cv2
import zed
import os


def main():

    cam = zed.ZEDCamera(resolution_mode=zed.VGA)

    cv2.namedWindow('Depth')
    while cv2.waitKey(1) < 0:
        t0 = time.time()

        while cam.grab(zed.FULL):
            continue

        normalized = cam.normalizeDepth()
        cv2.imshow('Depth', normalized)

        print 1/(time.time()-t0)

    cv2.destroyAllWindows()


if __name__ == '__main__':
    main()
