#!/usr/bin/env python
from __future__ import division
import numpy as np
import cv2
import zed


def main():

    cam = zed.ZEDCamera(zed.VGA)

    while cam.grab(zed.FULL):
        pass

    print 'image size:', cam.image_size
    print 'depth clamp:', cam.depth_clamp_value

    cv2.namedWindow('Left Image')
    cv2.imshow('Left Image', cam.retrieveImage())

    cv2.namedWindow('SBS')
    cv2.imshow('SBS', cam.getView())

    cv2.namedWindow('Depth')
    depth = cam.retrieveDepth()
    cv2.imshow('Depth', (depth/depth.max()*255).astype(np.uint8))

    cv2.namedWindow('Normalized')
    normalized = cam.normalizeDepth(1000, 5000)
    print normalized.shape
    cv2.imshow('Normalized', normalized)

    cv2.waitKey(0)


if __name__ == '__main__':
    main()
