from setuptools import setup, find_packages
from distutils.sysconfig import get_python_lib
import glob
import os
import sys


setup(
    name="zed",
    packages=find_packages(),
    data_files=[(get_python_lib(), glob.glob('src/*.so'))],
    author='Amit Aides',
    description='Cython wrapper for the ZED camera.',
)
